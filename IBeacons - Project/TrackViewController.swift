//
//  ViewController.swift
//  IBeacons - Detector
//
//  Created by Elias Gabriel dos Santos Correa on 09/03/19.
//  Copyright © 2019 Elias Gabriel dos Santos Correa. All rights reserved.
//

import UIKit
import CoreLocation

class TrackViewController: UIViewController {
    
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelIBeaconsDetected: UILabel!
    @IBOutlet weak var labelDistanceStatus: UILabel!
    
    
    let locationManager = CLLocationManager()
    
    func monitorBeacons() {
        if CLLocationManager.isMonitoringAvailable(for:
            CLBeaconRegion.self) {
            // Match all beacons with the specified UUID
            let proximityUUID = UUID(uuidString:
                "39ED98FF-2900-441A-802F-9C398FC199D2")
            let beaconID = "com.detector.myBeaconRegion"
            
            // Create the region and begin monitoring it.
            let region = CLBeaconRegion(proximityUUID: proximityUUID!,
                                        identifier: beaconID)
            self.locationManager.startMonitoring(for: region)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        monitorBeacons()
    }
    
    
}

extension TrackViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager,
                         didEnterRegion region: CLRegion) {
        if region is CLBeaconRegion {
            // Start ranging only if the feature is available.
            if CLLocationManager.isRangingAvailable() {
                manager.startRangingBeacons(in: region as! CLBeaconRegion)
                
                // Store the beacon so that ranging can be stopped on demand.
                //beaconsToRange.append(region as! CLBeaconRegion)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didRangeBeacons beacons: [CLBeacon],
                         in region: CLBeaconRegion) {
        labelIBeaconsDetected.text = "\(beacons.count)"
        if beacons.count > 0 {
            let nearestBeacon = beacons.first!
            //            let major = CLBeaconMajorValue(truncating: nearestBeacon.major)
            //            let minor = CLBeaconMinorValue(truncating: nearestBeacon.minor)
            switch nearestBeacon.proximity {
            case .immediate:
                // Display information about the relevant exhibit.
                labelStatus.text = "Detected IBeacon"
                labelDistanceStatus.text = "Immediate"
                break
            case .near:
                labelStatus.text = "Detected IBeacon"
                labelDistanceStatus.text = "Near"
                break
            case .far:
                labelStatus.text = "Detected IBeacon"
                labelDistanceStatus.text = "Far"
                break
            default:
                // Dismiss exhibit information, if it is displayed.
                labelStatus.text = "Unknown"
                labelDistanceStatus.text = "Unknown"
                break
            }
        }
    }
}


