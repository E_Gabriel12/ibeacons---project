//
//  TransmitViewController.swift
//  IBeacons - Project
//
//  Created by Elias Gabriel dos Santos Correa on 13/03/19.
//  Copyright © 2019 Elias Gabriel dos Santos Correa. All rights reserved.
//
import UIKit
import CoreLocation
import CoreBluetooth

class TransmitViewController: UIViewController {
    
    var beaconRegion : CLBeaconRegion! = nil
    var beaconPeripheralData : NSDictionary! = NSDictionary()
    var peripheralManager : CBPeripheralManager! = CBPeripheralManager()
    
    func createBeaconRegion() -> CLBeaconRegion? {
        let proximityUUID = UUID(uuidString:
            "39ED98FF-2900-441A-802F-9C398FC199D2")
        let major : CLBeaconMajorValue = 100
        let minor : CLBeaconMinorValue = 1
        let beaconID = "com.detector.myDeviceRegion"
        
        return CLBeaconRegion(proximityUUID: proximityUUID!,
                              major: major, minor: minor, identifier: beaconID)
    }
    
    func advertiseDevice(region : CLBeaconRegion) {
        let peripheral = CBPeripheralManager(delegate: self, queue: nil)
        let peripheralData = region.peripheralData(withMeasuredPower: nil)
        
        peripheral.startAdvertising(((peripheralData as NSDictionary) as! [String : Any]))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beaconRegion = createBeaconRegion()
        beaconPeripheralData = beaconRegion.peripheralData(withMeasuredPower: nil)
        peripheralManager = CBPeripheralManager.init(delegate: self, queue: nil)
    }
    
    
}

extension TransmitViewController: CBPeripheralManagerDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == .poweredOn {
            peripheralManager.startAdvertising(beaconPeripheralData as? [String : Any])
            print("Powered On")
        }
        else {
            peripheralManager.stopAdvertising()
            print("Not Powered On, or some other error")
        }
    }
}
